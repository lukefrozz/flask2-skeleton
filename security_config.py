from flask_security import Security, SQLAlchemyUserDatastore, ConfirmRegisterForm
from wtforms import StringField
from wtforms.validators import DataRequired

from db_config import db
from app.mod_auth.models import User, Role


class ExtendedRegisterForm(ConfirmRegisterForm):
    extra = StringField('extra', [DataRequired()])


user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(confirm_register_form=ExtendedRegisterForm)

