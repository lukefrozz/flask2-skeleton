from flask_jwt_extended import create_access_token
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from sqlalchemy import or_, alias
from sqlalchemy.orm import aliased

from ..models import Matchup, Champion
from db_config import db
from flask import request,  Blueprint
from werkzeug.security import check_password_hash

from ...mod_team.models import Team, Player

bp = Blueprint('champion', __name__, url_prefix='/champion')


@bp.get('')
def get_champions():
    args = []
    if request.args.get('s'):
        args.append(Champion.name.contains(request.args['s']))
    champions = Champion.query.filter(*args).order_by(Champion.name)

    return {'champions': [champion.to_dict() for champion in champions]}
