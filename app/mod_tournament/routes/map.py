from flask_jwt_extended import create_access_token
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from sqlalchemy import or_, alias
from sqlalchemy.orm import aliased

from ..models import Matchup, MatchupMap
from db_config import db
from flask import request,  Blueprint
from werkzeug.security import check_password_hash

from ...mod_team.models import Team, Player

bp = Blueprint('map', __name__, url_prefix='matchup/<int:matchup_id>/map')


@bp.post('')
def post_map(matchup_id: int):
    matchup = Matchup.query.get(matchup_id)
    map = MatchupMap(**{**request.json, 'tournament_id': matchup.tournament_id})
    map.matchup_id = matchup_id
    map.map_number = len(matchup.maps) + 1
    db.session.add(map)
    db.session.commit()

    return {
        'map_id': map.id
    }, 201

