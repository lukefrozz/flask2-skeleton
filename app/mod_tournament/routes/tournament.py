from flask_jwt_extended import create_access_token
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from sqlalchemy import or_, alias
from sqlalchemy.orm import aliased

from ..models import Tournament, TournamentTeam
from db_config import db
from flask import request,  Blueprint
from werkzeug.security import check_password_hash

from ...mod_team.models import Team, Player

bp = Blueprint('tournament', __name__, url_prefix='/tournament')


@bp.get('')
def get_tournaments():
    args = []
    if request.args.get('s'):
        args.append(Tournament.name.contains(request.args['s']))
    tournaments = Tournament.query.filter(*args).order_by(Tournament.name)

    return {'tournaments': [tournament.to_dict() for tournament in tournaments]}


@bp.post('')
def post_tournament():
    data = {**request.json}
    if 'id' in data:
        del data['id']
    lineups = []
    if 'lineups' in data:
        lineups = data['lineups']
        del data['lineups']
    tournament = Tournament(**data)
    db.session.add(tournament)
    db.session.commit()

    for lineup in lineups:
        players = lineup['players']
        del lineup['players']
        t_team = TournamentTeam(**{**lineup, 'tournament_id': tournament.id})
        db.session.add(t_team)
        db.session.commit()
        for player in players:
            t_team.players.append(Player.query.get(player))

    db.session.commit()
    return {
        'tournament': tournament.id
    }
