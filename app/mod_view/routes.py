from flask_jwt_extended import create_access_token
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from sqlalchemy import or_

from .models import Team, Player
from db_config import db
from flask import request, render_template, Blueprint
from werkzeug.security import check_password_hash

bp = Blueprint('team', __name__, url_prefix='/team')


@bp.get('')
def get_team():
    teams = Team.query.all()
    return teams
