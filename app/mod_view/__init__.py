from flask import Blueprint


def bp():
    _bp = Blueprint('view', __name__, url_prefix='/v1/view')

    from .routes import champion
    _bp.register_blueprint(champion.bp)
    from .routes import player
    _bp.register_blueprint(player.bp)

    return _bp
