from flask_jwt_extended import create_access_token
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from sqlalchemy import or_, Column

from ..models import Team, Player
from db_config import db
from flask import request, render_template, Blueprint
from werkzeug.security import check_password_hash

from ...mod_tournament.models import Tournament, TournamentTeam

bp = Blueprint('team', __name__, url_prefix='/team')


@bp.get('')
def get_team():
    args = []
    joins = []
    if request.args.get('phase'):
        args.append(Team.phase == request.args['phase'])
    if request.args.get('t'):
        joins.append((TournamentTeam, TournamentTeam.team_id == Team.id))
        joins.append((Tournament, Tournament.id == TournamentTeam.tournament_id))
        args.append(Tournament.id == int(request.args['t']))
    if len(joins):
        teams = Team.query.with_entities(
            Team.id, Team.flag, Team.name, Team.phase, Team.tag,
        ).outerjoin(*joins).filter(*args)
    else:
        teams = Team.query.with_entities(
            Team.id, Team.flag, Team.name, Team.phase, Team.tag,
        ).filter(*args)

    return {'teams': [dict(id=team.id, flag=team.flag, name=team.name, phase=team.phase,
                           tag=team.tag, label=f'{team.tag} - {team.name}')
            for team in teams]}


