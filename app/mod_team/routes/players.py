from flask_jwt_extended import create_access_token
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from sqlalchemy import or_, Column

from ..models import Team, Player
from db_config import db
from flask import request, render_template, Blueprint
from werkzeug.security import check_password_hash

bp = Blueprint('player', __name__, url_prefix='player')
roles = {
    '1': 'baron',
    '2': 'jungle',
    '3': 'mid',
    '4': 'dragon',
    '5': 'sup'
}


@bp.get('')
def get_players():
    args = []
    players = Player.query.with_entities(
        Player.id, Player.nickname, Player.flag, Player.team_id, Player.role,
    ).filter(*args)

    return {'players': [dict(
        id=player.id, nickname=player.nickname, flag=player.flag, team_id=int(player.team_id), role=None if not player.role else player.role.name)
        for player in players]}


