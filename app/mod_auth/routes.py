from flask_jwt_extended import create_access_token
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from sqlalchemy import or_

from . import bp
from .models import User, Role
from db_config import db
from flask import request, render_template
from werkzeug.security import check_password_hash
