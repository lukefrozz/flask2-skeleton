 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.blue_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.blue_ban_1 AS ban_id,
    matchup_map.blue_pick_1 AS pick_id,
    'blue'::text AS side,
    1 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.blue_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.blue_ban_2 AS ban_id,
    matchup_map.blue_pick_2 AS pick_id,
    'blue'::text AS side,
    2 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.blue_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.blue_ban_3 AS ban_id,
    matchup_map.blue_pick_3 AS pick_id,
    'blue'::text AS side,
    3 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.blue_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.blue_ban_4 AS ban_id,
    matchup_map.blue_pick_4 AS pick_id,
    'blue'::text AS side,
    4 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.blue_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.blue_ban_5 AS ban_id,
    matchup_map.blue_pick_5 AS pick_id,
    'blue'::text AS side,
    5 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.red_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.red_ban_1 AS ban_id,
    matchup_map.red_pick_1 AS pick_id,
    'red'::text AS side,
    1 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.red_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.red_ban_2 AS ban_id,
    matchup_map.red_pick_2 AS pick_id,
    'red'::text AS side,
    2 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.red_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.red_ban_3 AS ban_id,
    matchup_map.red_pick_3 AS pick_id,
    'red'::text AS side,
    3 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.red_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.red_ban_4 AS ban_id,
    matchup_map.red_pick_4 AS pick_id,
    'red'::text AS side,
    4 AS sequence
   FROM matchup_map
UNION ALL
 SELECT uuid_generate_v4() AS id,
    matchup_map.id AS map_id,
    matchup_map.matchup_id,
    matchup_map.red_side AS team_id,
    matchup_map.winner,
    matchup_map.winner_side,
    matchup_map.length,
    to_seconds(matchup_map.length::text) AS length_sec,
    matchup_map.red_ban_5 AS ban_id,
    matchup_map.red_pick_5 AS pick_id,
    'red'::text AS side,
    5 AS sequence
   FROM matchup_map;