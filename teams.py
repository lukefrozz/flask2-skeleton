from typing import List
from app import db, create_app
import wikitextparser as wtp
import json

from app.mod_team.models import Team, Player
from app.mod_tournament.models import Tournament, TournamentTeam

create_app().app_context().push()


def create_all_teams(data: List[dict]) -> dict:
    teams_obj = []
    players_obj = []
    for team in data:
        team_obj = Team(name=team['team'], tag='', flag=team['flag'], phase=team['phase'])
        db.session.add(team_obj)
        db.session.commit()
        teams_obj.append(team_obj)
        baron = Player(nickname=team['p1'].split(' ')[0], flag=team.get('p1flag'), team_id=team_obj.id, role='baron')
        jungle = Player(nickname=team['p2'].split(' ')[0], flag=team.get('p2flag'), team_id=team_obj.id, role='jungle')
        mid = Player(nickname=team['p3'].split(' ')[0], flag=team.get('p3flag'), team_id=team_obj.id, role='mid')
        dragon = Player(nickname=team['p4'].split(' ')[0], flag=team.get('p4flag'), team_id=team_obj.id, role='dragon')
        sup = Player(nickname=team['p5'].split(' ')[0], flag=team.get('p5flag'), team_id=team_obj.id, role='sup')

        db.session.add(baron)
        db.session.add(jungle)
        db.session.add(mid)
        db.session.add(dragon)
        db.session.add(sup)
        players_obj.append(baron)
        players_obj.append(jungle)
        players_obj.append(mid)
        players_obj.append(dragon)
        players_obj.append(sup)
        substitutes = list(set([key[0:2] for key in team.keys() if key.startswith('t2')]))
        for index, sub in enumerate(substitutes):
            _s = f'{sub}p{index+1}'
            sub = Player(
                nickname=team[_s].split(' ')[0], flag=team.get(f'{_s}flag'),
                team_id=team_obj.id, role=team[f'{sub}pos{index+1}']
                if team[f'{sub}pos{index+1}'] in ['baron', 'jungle', 'mid', 'dragon', 'sup'] else None
            )
            db.session.add(sub)
            players_obj.append(sub)

    db.session.commit()
    db.session.close()

    return dict(teams=teams_obj, players=players_obj)


group = open('liquifiles/icons_group_teams.txt', mode='r').read()
play_in = open('liquifiles/icons_playin_teams.txt', mode='r').read()
parsed_group = wtp.parse(group)
parsed_play_in = wtp.parse(play_in)

sections = []
sec_dict = []
for index, temp in enumerate(parsed_group.templates):
    if '{{TeamCard\n' in temp.string:
        # print(f'{index} -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
        # print(temp)
        # sections.append(temp)
        team = {arg.name: arg.value.strip() for arg in temp.arguments}
        team['phase'] = 'group'
        sec_dict.append(team)

for index, temp in enumerate(parsed_play_in.templates):
    if '{{TeamCard\n' in temp.string:
        # print(f'{index} -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
        # print(temp)
        # sections.append(temp)
        team = {arg.name: arg.value.strip() for arg in temp.arguments}
        team['phase'] = 'play-in'
        sec_dict.append(team)
# print(sections)
print(json.dumps(sec_dict, indent=2))

create_all_teams(sec_dict)

t = Tournament(
    name='Icons Global Championship 2022', tag='icons', region='world',
    start_date='2022-06-14', end_date='2022-07-09', split=1,
    phases=['play-in', 'group', 'knockout']
)

db.session.add(t)
db.session.commit()

teams = list(Team.query.filter(Team.id > 5))
players = list(Player.query.filter(Player.id > 35))

for team in teams:
    t_team = TournamentTeam(tournament_id=t.id, team_id=team.id, entry_phase=team.phase)
    db.session.add(t_team)
    db.session.commit()
    t_team.players = [p for p in players if p.team_id == team.id]
    db.session.commit()
